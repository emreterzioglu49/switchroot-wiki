# FAQ

## Switchroot and Linux4Switch

Switchroot is a group for open-source development on the Nintendo Switch, a Tegra X1-based game console with a FOSS bootstrap exploiting a low-level recovery bootloader.

As opposed to homebrew communities, Switchroot is focused on baremetal, low-level and kernel development, including bootloaders, hypervisors, Linux and other OS ports, drivers and I/O, and low-level development with, and modification of, Horizon, the Switch game OS.

Linux4Switch is another open-source development group on the Nintendo Switch, which focused on low-level and kernel development, including bootloaders, hypervisors, Linux and other OS ports, drivers and I/O, and low-level development.

### Q/A

- Can I run steam and other x86(_64) apps ?

> No, the Switch CPU architecture is aarch64 and unless some support is brought for other architectures by those companies to their apps you won't be able to run steam and other x86(_64) apps natively (see more on [emulation](https://github.com/linux-4-switch/wiki/emulation.md) )

- Can emulate my legally owned game backups within Android or Linux ?

> Many console emulators are open sourced and/or have native support for aarch64. Projects such as RetroArch and RetroPie have native support.

- How do I access my SWITCH SD from PC ?

With your Switch connected to your PC with a USB C -> A Cable:
> In Hekate \
> Tools -> USB Tools -> USB Mass Storage > SD Card

Within Linux (network share):
> Setup SMB/Samba sharing on Windows or Linux (many right ways to do so) \
> Mount the share in Linux on Switch (cifs-utils or gigolo)

## Other references and Links

[Switchroot Website](https://switchroot.org) \
[Switchroot Gitlab](https://gitlab.com/switchroot/) \
[Linux4Switch Github](https://github.com/Linux-4-Switch)

## Wiki pages

[Android](./android/README.md) \
[Bootstack](./bootstack/README.md) \
[Kernel](./kernel/build.md) \
[userland](./userland/README.md)

