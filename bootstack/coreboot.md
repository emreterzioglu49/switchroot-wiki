# Coreboot

[Coreboot-Switch](https://gitlab.com/switchroot/bootstack/coreboot-switch)


## Requirements

GCC 7.4.0

## Pre build

Coreboot requires to have arm and aarch64 compilers as the switch is both arm and arm64 architecture.
You have 2 choice, either compile coreboot cross compilers or use Linaro pre built cross compilers.
If you chose to build corebooot's cross compilers follow this :

Building arm cross compiler :

`make crossgcc-arm`

Building aarch64 cross compiler :

`make crossgcc-aarch64`

## Building

`make nintendo_switch_defconfig`
`make iasl`

**Use only 2 threads as it won't build with more threads.**
`make -j2`
