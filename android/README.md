# Android userland

## Current Release: Android Q/10

[Download Android Q here](https://download.switchroot.org/android-10/).

You can build Q using either:
[PabloZaiden's docker scripts](https://github.com/PabloZaiden/switchroot-android-build) 
Or [Joelgp83 bash scripts](https://github.com/Joelgp83/Build-scripts-for-switchroot-android)
Both repositories are based on [ZachyCatGames guide](https://gitlab.com/ZachyCatGames/q-tips-guide)


## Current Beta version: Android R/11

You can build R by following the [Rene-Guide](https://gitlab.com/makinbacon17/rene-guide).

## Tips and tricks

### Magisk

To use Magisk with Android Q, you can download the latest release from [here](https://github.com/topjohnwu/Magisk/releases).

Rename the apk file to a zip file so we can flash it in TWRP.

Reboot to Hekate from Android Q by pressing down Volume - when you are about to reboot. Keep holding it down until you get to the Hekate screen.

Once you are in Hekate, hold down Volume - before you click the config to boot into Android Q. You should now be booting into TWRP.

When you get into TWRP, stop holding down Volume -. Slide the slider to allow changes. Press install and find the Magisk zip. Click on it, and slide the slider to flash the zip.

When it's done, reboot back into Android Q, and you should be done! Enjoy Magisk on Android Q!

### Using dock in Q (voodoo dock method)

#### Note: This is untested with the release of Android Q. Try at your own risk.

For those who want to use the dock with Q but don't want to turn off HW overlays, you can do so by docking the switch before the boot animation plays.
You can dock and undock to your heart's content until your next reboot (just dock while booting again), though after some time, you may see essentially "ghost" images on the dock output (home screen icons, stuff from previously opened apps, etc).
At that point, you should probably reboot, as android will not stay up much longer after you see it. 
Also, don't dock > undock > dock too quickly, as that can cause issues. 

