# USB UART

For USB UART enable the following options in kernel :

```txt
CONFIG_USB_G_SERIAL=y
CONFIG_U_SERIAL_CONSOLE=y
CONFIG_USB_SERIAL_CONSOLE=y
```

Then make sure all configfs gadgets are disabled and rebuild the kernel.
