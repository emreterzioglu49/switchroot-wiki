# Update boot files

The boot files are handled in gentoo by ebuilds. By default all required files are installed into `/usr/share/sdcard1`. 
The sdcard-mount.eclass provides additional function to install the files directly in the vfat partition.

This guide show you how it can be set up and how it works.

## Setting up a mount point

In any case a mount point is required. For example /media/SWITCH_SD

```
mkdir /media/SWITCH_SD
echo '/dev/mmcblk0p1 /media/SWITCH_SD vfat defaults 0 0' >> /etc/fstab
mount /media/SWITCH_SD
```

## Updating manually

The files in `/usr/share/sdcard1` are updated by ebuilds on your daily system update.
Just copy/sync the updated files from `/usr/share/sdcard1` to `/media/SWITCH_SD`.

## Enable auto-updates with portage

Add the next lines to the `/etc/portage/make.conf`. If the file does not exist, create it.

```
SDCARD_MOUNT_POINT="/media/SWITCH_SD"
CONFIG_PROTECT="$SDCARD_MOUNT_POINT"
```

If this setting is set, the files are installed in both folders, in `/usr/share/sdcard1` and `/media/SWITCH_SD` by ebuild.
The CONFIG_PROTECT line is required to avoid silent overrides of your own settings. If set, you need to apply changes using `etc-update` or `dispatch-conf`.

## Update hekate NYX files by portage

Just set the USE flag : `USE="nyx"` for `sys-boot/reboot2hekate-bin` and rebuild it.

