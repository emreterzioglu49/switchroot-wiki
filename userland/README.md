# GNU/Linux distribution

Various linux distributions are avalaible to build.
You can use [Jet-Factory](https://github.com/linux-4-switch/Jet-Factory) to build a distribution for your Switch.

The state of each distributions avalaible is variable here is a little table reporting the state of it :

| Distro 	| State  | Version |
|---------------|------------|---------|
| Arch   	| Open Beta	 |  latest |
| Bionic 	| Stable |  18.04  |
| Fedora 	| Open Beta  |   32    |
| Focal	 	| Stable | 20.04.1 |
| Gentoo	| Stable |    -    |
| Groovy	| Stable |  20.10  |
| Tumbleweed	| Alpha  |    -    |

## Distributions layout

```txt
bootloader/ini
     |_______Switchroot-XXXXXXX.ini

switchroot
  |_______install
	  |_______l4t.00        (4092 MiB parts. Last part can be 4 MiB aligned)
	  |_______l4t.01
	  ...
	  |_______l4t.XX
   |_______ubuntu
	   |_______boot.scr
	   |_______coreboot.rom
	   |_______Image
	   |_______initramfs
	   |_______tegra210-icosa.dtb
```

## Flashing a distribution

:First time Linux installation red:

* 1. Partition the sd card in order to create an ext4 partition.
hekate's partition manager can be used for that: Tools -> 2nd tab on the bottom -> Partition SD Card.

The process in hekate is destructive, so you need to backup your fat partition files (if they are more than 1GB total) or emuMMC.

*Move the sliders* as you want them and then hit next. You will have plenty of chances/warns to backup your stuff if it's needed.

* 2. *Extract* the 2GB (standard) or the 4GB (developer cuda) 7z directly to SD
* 3. Safely unmount and flash via hekate's *Flash Linux*
* 4. Then go to Home and Nyx options and dump your joycon BT pairing
* 5. boot

## Note: The other pages in this folder are empty at the moment.